package com.mastertech.autenticacao.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.mastertech.autenticacao.model.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Long>{
	public Optional<Usuario> findByEmail(String email);
}
